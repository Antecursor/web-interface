function handleFileSelect(evt) {
    // get the selected file
    var f = evt.target.files[0];

    // transform selected file into sql.js data (ByteArray)
    var r = new FileReader();
    r.readAsArrayBuffer(f);
    r.onload = function () {
        Uints = new Uint8Array(r.result);
        db = new SQL.Database(Uints);

        displayDB(db);
    };
}

function displayDB(db){
    res = db.exec('SELECT ip_address, port_number, protocol FROM Port ORDER BY ip_address;');
    host_data = {}
    res[0].values.forEach(function (row) {
        [ip_address, port, protocol] = row;
        if (!(ip_address in host_data)) {
            host_data[ip_address] = {'ports': [], 'creds': []};
        }
        host_data[ip_address]['ports'].push({'port': port, 'protocol': protocol});
    });

    res = db.exec('SELECT DISTINCT dst_ip, dst_port, type, data FROM Credentials ORDER BY dst_ip;');
    res[0].values.forEach(function (row) {
        [ip_address, port, type, creds] = row;
        if (!(ip_address in host_data)) {
            host_data[ip_address] = {'ports': [], 'creds': []};
        }
        host_data[ip_address]['creds'].push({'type': type, 'creds': creds.replace(/[() ']/g, '').split(',')});
    });

    html_output = '';
    for (var host in host_data) {
        html_output += `<div class="card border-secondary mb-3" style="max-width: 20rem;"><div class="card-header">${host}</div><div class="card-body"><p>`;
        for (var host_port in host_data[host]['ports']) {
            [port, protocol] = [host_data[host]['ports'][host_port]['port'], host_data[host]['ports'][host_port]['protocol']];
            html_output += `<span class="badge badge-secondary">${port} (${protocol})</span>`;
        }
        for (var host_creds in host_data[host]['creds']) {
            [login, password] = host_data[host]['creds'][host_creds]['creds'];
            service_type = host_data[host]['creds'][host_creds]['type'];
            html_output += `<span class="badge badge-danger">${login}:${password} (${service_type})</span>`;
        }
        html_output += '</p></div></div>';
    }
    document.getElementById('body').innerHTML = html_output;
}

document.getElementById('upload').addEventListener('change', handleFileSelect, false);